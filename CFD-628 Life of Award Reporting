// https://childfundinternational.atlassian.net/browse/CFD-628
// Life of Award Reporting
// Sprint 2

OPTION 1: VIEW REPORT ON EXISTING PROJECT
A. View Life of Award FYTD Report
1. Log in as a 'Grants' user.
2. From the app selector (waffle icon in the upper-left), search for and select 'Projects'.
2.1. Search for and open the 'KEN-Early Childhood Development' project https://cfconnect--uat.lightning.force.com/lightning/r/Project__c/a1a63000001RxyzAAC/view
3. On the Project 'Details' sub-tab, note the values in the 'Project Finances FYTD' section.
4. At the top of the Project's 'Details' sub-tab, next to 'Life of Award FYTD Report', click the 'Click Here' link.
5. Validate the report values. 

OPTION 2: CREATE NEW RECORDS
A. Create a Project.
1. Log in as a 'Grants' user.
2. From the app selector (waffle icon in the upper-left), search for and select 'Projects'.
3. Click the 'New' button.
3.1. Select 'Single Country Project' and click 'Next'.
3.2. Enter a name in the 'Project' field.
3.3. Fill in the other required fields. (click 'Save' to have the system show the required fields). 
3.4. Click 'Save'.

B. Create a Cost Extension Funding
1. Click on the Project's (from section C above) 'Related' sub-tab.
2. Next to 'Funding', click 'New'.
2.1. Leave Opportunity blank.
2.2. Select 'Cost Extension' in the 'Type' field.
2.3. Fill in other required fields.
2.4. Click 'Save'.

C. Activate Funding Import
1. Log in as an Admin, or as Ayesha Brown.
2. Open the Cost Extension Funding record created in section B above.
3. Check the 'Funding Import' box and save the record.

D. View Life of Award FYTD Report
1. Log in as a 'Grants' user.
2. On the Project 'Details' sub-tab, note the values in the 'Project Finances FYTD' section.
3. At the top of the Project's 'Details' sub-tab, next to 'Life of Award FYTD Report', click the 'Click Here' link.
4. Validate the report values. 

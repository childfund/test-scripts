// https://childfundinternational.atlassian.net/browse/CFD-1846
// Special Substitution - Participant Automation
// Sprint9

A. Open a Donor Contact
1. Log in as a 'Country Office' user.
2. Open or create a Donor Contact.

B. Create a Sponsorship Donation
1. Click 'New Sponsorship' at the top of the Contact page.
1.1. Fill in the required and optional fields and click 'Save'. 
2. Click the Contact's 'Related' sub-tab.
3. Click on the new sponsorship in the 'Recurring Donation' list.

C. Update Substitute Participant
1. Click 'Update Substitute Participant' on the top of the Recurring Donation page.
2. Select a Participant and click 'Next'.
2.1. Validate the Participant is displayed in the Recurring Donation's 'Substitute Participant' field. 
3. Click on the Substitute Participant's name to open the record.
3.1. Validate 'Substitute for Sponsorship' is populated with the Recurring Donation
3.2. Validate the Participant's 'Sponsorship Status' is 'Held as Substitute'.
4. Note this Participant's name. You will be opening the record again later.

C. Update Substitute Participant
1. Open the Sponsorship Recurring Donation created in Section B.
2. Click 'Update Substitute Participant' on the top of the Recurring Donation page.
3. Select a *Different* Participant and click 'Next'.
2.1. Validate the new Participant is displayed in the Recurring Donation's 'Substitute Participant' field. 
3. Click on the Substitute Participant's name to open the record.
3.1. Validate 'Substitute for Sponsorship' is populated with the Recurring Donation
3.2. Validate the Participant's 'Sponsorship Status' is 'Held as Substitute'.
4. Open the Participant record from Section C above.
4.1. Validate 'Substitute for Sponsorship' is blank.
4.2. Validate the Participant's 'Sponsorship Status' is released from being held (Status depends on multiple factors, and may not be 'Available').

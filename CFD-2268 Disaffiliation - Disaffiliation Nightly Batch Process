// https://childfundinternational.atlassian.net/browse/CFD-2268
// Disaffiliation - Disaffiliation Nightly Batch Process
// Sprint13

Note: The Disaffiliation process consists of several stories. This story covers the departure of sponsored children, and children in a reserve. 

Log in as a Sponsorship user.

A. Open a Community Account with Sponsored Participants
1. Click on the 'Participants' tab. 
2. Select the 'Sponsored Participants' list view.
3. Select a Community with one or more Participants in the list view. 
4. Confirm the Community Account is Active and does not have a 'Disaffiliation Stage'.
5. Note the name of one or more Sponsored or Pre-Sponsored Participants. 

B. Unplanned Disaffiliation
1. Click the 'Manage Account' button in the upper right.
1.1. Select 'Manage Disaffiliation', and click 'Next'.
2. For 'Disaffiliation Type', select 'Unplanned', and click 'Next'.
2.1. Accept or edit 'Additional Disaffiliation Departure Details'.
2.2. Enter a 'Disaffiliation Date' of today, and click 'Next'.

C. Participant Sponsorship Status
1. Check the Notifications (upper-right next to profile avatar) for an alert that the 'Disaffiliation - process completed successfully.'
2. Click on the 'Related' sub-tab.
3. Refresh the 'Participants (Community)' list.
4. Select a Participant from Step A.5, now with a status of 'Departed'.
5. Click on the Participant's 'Departure Details' sub-tab. 
6. Validate the 'Departure Reason' is 'Community Exit'
7. Validate the field entries match what was entered in Section B. 

D. Reserved Participants
Repeat the steps above. For Step A.2. choose the list view 'Reserved Participants'.

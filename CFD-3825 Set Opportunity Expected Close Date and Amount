https://childfundinternational.atlassian.net/browse/CFD-3825
Set Opportunity Expected Close Date and Amount
Sprint40

As a Sponsor Care user
1. Create a donation (Sponsorship, Contribution, Designated Fund, both one-time and recurring)
2. Update the date and amounts via the "Update Donations" on the donor's page.
3. On an Open Opportunity, confirm the Expected Date and Expected Amount change per the AC
4. On a Closed Opportunity, confirm the Expected Date and Expected Amount do NOT change, per the AC. For this the user will need to edit the Opportunity fields directly, not via automation.

6. From the Contribution, [Manage]
7. Choose 'Update Schedule', and change the day of the month
8. Open the open Opportunity. Amount = Expected Amount
- Opp Stage = Completed with different amount.
9. After the Opportunity is paid, open it again. Expected Amount does not change. 

A. Create A Recurring Contribution
Log in as a Sponsor Care user
1. Open a donor Contact or Organization Account.
2. Click [New Sponsorship/Donation].
3. For 'Select an Action', choose "Contribution (One-Time or Recurring)", and click [Next].
3.1. For 'Contribution Type', make a selection, and click [Next].
3.2. Fill in the donation information...
3.2.1. For 'Monthly Standard Pay', enter an amount.
3.2.2. For 'Installment Period', select an interval (Monthly). 
3.2.3. Enter other required fields and optional fields if desired, and click [Next].
3.3. If necessary, fill in a new payment method, and click [Next].
3.4. On the confirmation message, click [Finish].
3. On the recurring Contribution (from the donor's Products related list).
4. Open the installment Opportunity (from the Contribution's 'Opportunities' tab).
4.1. Confirm that "Amount" = "Expected Amount".
- "Amount" is the Salesforce field that may change over the lifecycle of the Opportunity.
- "Expected Amount" provides the user a consistent view for the anticipated payment.
4.1. Confirm that "Close Date" = "Expected Close Date".
- "Close Date" is the Salesforce field that may change over the lifecycle of the Opportunity.
- "Expected Close Date" provides the user a consistent view for when the anticipated payment.

B. Change Contributions Installment Period
Continue as the Sponsor Care user on the Contribution from Section A.
1. Click [Manage].
1.1. Select 'Update Schedule', and click [Next].
1.2. For 'Installment Period', select a different interval, and click [Next].
2. Open the updated installment Opportunity.
2.1. Confirm that "Amount" = "Expected Amount".
2.2. Confirm that "Close Date" = "Expected Close Date".
- Because the installment is pending/open the Salesforce and Expected values should be the same.
3. Repeat for other installment periods.

C. Change Contributions Monthly Standard Pay
Continue as the Sponsor Care user on the Contribution from Section A and B.
1. Click [Manage].
1.1. Select 'Update Monthly Standard Pay', and click [Next].
1.2. For 'Monthly Standard Pay', enter a new amount, and click [Next].
2. Open the updated installment Opportunity.
2.1. Confirm that "Amount" = "Expected Amount".
2.2. Confirm that "Close Date" = "Expected Close Date".
- Because the installment is pending/open the Salesforce and Expected values should be the same.

D. Complete an Installment Opportunity
- The Sponsor Care user can distribute money to the installment via the Distribution Screen or the Adjustments & Refunds screens. If doing so, choose an Amount less than the expected amount and continue on Step 3. The steps below are for a System Administrator to simulate without actually applying payment. 
Log in as a System Administrator
1. Open the installment Opportunity from above.
2. For 'Stage', click the pencil icon to edit.
2.1. For 'Stage', select "Completed".
2.2. For 'Amount', enter a value less than the current amount.
2.3. For 'Close Date', enter an earlier date.
2.4. Click [Save].
- This closes the installment as if a payment was received on a date earlier than expected, for an amount less than expected.
3. Open the updated installment Opportunity.
3.1. Confirm that "Amount" = the actual payment received, and "Expected Amount" remains the same.
3.2. Confirm that "Close Date" = the actual date payment was received, and "Expected Close Date" remains the same.
- Because the installment has closed the Salesforce updates the Close Date and Amount to the actual values, and Expected values should be the same so the user has the full context.
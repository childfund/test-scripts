https://childfundinternational.atlassian.net/browse/CFD-3569
Giving Frequencies for DFs
Sprint26

OVERVIEW
- Create Different recurring gifts for a donor, and confirm the giving frequency options include day of month, but not an "Installment Frequency".
- Change the schedule of recurring gifts, and confirm the giving frequency options include day of month, but not an "Installment Frequency".
Note: Updates to Designated Funds will occur in a future Sprint
Note: Because of NPSP requirements, changing the value from "Yearly" to "Annually" is not an option.
Note: Because of NPSP requirements, it is not possible to update the items displayed in the Recurring Donation "Active Schedules" side component.

TEST STEPS

A. Create a Donor Contact Sponsorship
Log in as a Sponsor Care user
1. Open or create a donor Contact.
2. Click [New Sponsorship/Donation] at the top of the Contact page.
2.1. Select 'Sponsorship (Recurring Donation)' and click [Next].
2.2. Select either 'Automatic Selection (No Preferences)' or 'Find a Child' and click [Next].
2.2.1. If 'Find a Child', select a Participant and click [Next].
2.3. Fill in the required fields
2.3.1. For 'Installment Period', select "Semi-Annual".
2.3.2. Confirm that a 'Installment Frequency' is not presented, but a 'Day of Month' is.
2.3.3. Choose a different 'Installment Period', to make the same confirmation.
2.3.4. Optionally click [Next], then [Finish] to create the Sponsorship.

B. Create a Non-Sponsorship Recurring Donation
Continue as the Sponsor Care user
1. Continue on the donor Contact.
2. Click [New Sponsorship/Donation] at the top of the Contact page.
2.1. Select 'Non-Sponsorship (Recurring Donation)' and click [Next].
2.2. Fill in the required fields
2.2.1. For 'Installment Period', select "Semi-Annual".
2.2.2. Confirm that a 'Installment Frequency' is not presented, but a 'Day of Month' is.
2.3.3. Choose a different 'Installment Period', to make the same confirmation.
2.3.4. Optionally click [Next] to create the DF.

C. Update Donations
Continue as the Sponsor Care user on the Donor page
1. Select the 'Products' sub-tab.
2. Select the 'Update Donations' sub-tab.
3. Select the 'Update/Schedule Opportunities' sub-tab.
3.1. Select 'Payment Schedule/Method', and click [Next].
3.2. For 'Installment Period', select "Semi-Annual".
3.3. Confirm that a 'Installment Frequency' is not presented, but a 'Day of Month' is.
3.4. Choose a different 'Installment Period', to make the same confirmation.
3.5. Optionally select required options and click [Next] to finish the schedule change.
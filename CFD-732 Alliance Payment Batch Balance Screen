https://childfundinternational.atlassian.net/browse/CFD-732
Alliance Payment Batch Balance Screen
Sprint31

Test A: Reject an Alliance Batch
As a Treasury user
1. Open an Alliance Batch in "Pending" status
2. Click [Manage Batch] from the top of the page.
2.1. Confirm the option to 'Balance' or 'Reject' are available.
2.2. For 'Select Available Option', choose "Reject", and click [Next].
2.3. Enter the required fields:
- Balance Rejection Reason
- Deposit Date
- Deposit Amount
2.4. Click [Next].
3. Click [Manage Batch] 
3.1. Confirm the option to 'Balance' or 'Reject' are no longer available.

Test B: Apply Exchange Rate to Alliance Batch
As a Treasury user 
1. Navigate to a Pending Alliance Batch
2. Click [Manage Batch] from the top of the page.
2.1. Confirm the option to 'Balance' or 'Reject' are available.
2.2. For 'Select Available Option', choose "Balance", and click [Next].
2.3. Confirm fields are auto-selected:
- Currency
- Balanced Date
- Exchange Rate Start Date
- Exchange Rate End Date
- Process Until Date (defaults to the Balanced Date + the Alliance’s Default Process Until Date)
2.4. Enter the required fields:
- Exchange Rate
- Deposit Amount
- Deposit Amount
2.5. Click [Next].
3. Confirm the following fields are updated on the Alliance Batch:
- 'Exchange Rate'
- 'Process Until Date'
- 'Balanced Date'
4. Confirm the option to 'Balance' or 'Reject' are no longer available in the [Manage Batch] button.

Test C: Exchange Rate Confirmation
As an Admin User
1. Open the Currencies tab
2. Open the currency used in the previous steps
3. From the 'Related' tab, review the 'Exchange Rates' related list
- Confirm that the exchange rate records are end-dated
- Confirm a new exchange rate was created from the action above
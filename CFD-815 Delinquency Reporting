https://childfundinternational.atlassian.net/browse/CFD-815
Delinquency Reporting
Sprint38

A. Delinquency Reports Reports
Log in as a Sponsor Care user
1. Click the 'Reports' tab.
2. Click 'All Folders' from the left column.
3. Select "Call Center Reports", and then the "Delinquency Reports" folder.
4. Select to run each of the reports.
These reports are dependent on SMART fields located on Donor Contact and Donor Organization Account records (in the Sponsorship SMART Fields section on Contact and Organization Account pages, the "# Active Delinquent Sponsorships" needs to be greater than 0 for any of the donors associated recurring donation records to show up. Those fields are not being populated yet, but we can update them manually for testing. Currently blank on all records.

There are two different styles of report:
- Style 1 - the all-in-one Donor Contact and Donor Organization Account reports:
"Delinquency Report: Donor Contacts"
"Delinquency: Organization Donors"
These reports include everything for Donors in single reports (all of the various groupings are in one report). This groups by Donor Number, then lists the recurring donation details, and then calculates if any of the segment information is true for use in filtering or grouping as required. Missing from these reports are E+F - the Case reports.

- Type 2 - Splitting the segments out into their own individual reports:
a. Suspense. Donor has any funds in “Suspense” GAU
Report: "Delinquency: Donor Contact with Suspense"
Requires the # Active Delinquent Sponsorships value on the contact to be >0 and requires one of that contacts recurring donations to be linked to a GAU with a Product Type of "Suspense".

b. Philanthropy Advisor. Contact’s parent Account has an an Account Team Member with a Role = “Philanthropy Advisor”
Report: "Delinquency: Donor Contact with PA"
Requires the # Active Delinquent Sponsorships value on the contact to be >0 and requires the "Philanthropy Advisor Count" on the Contact's Household account to be >0 (this can be set by going to the Household and adding an Account Team Member of type Philanthropy Advisor).

c. Organization. Sponsorship’s Account Record Type <> Household.
Report: "Delinquency: Organization Donors"
Requires the # Active Delinquent Sponsorships value on an Organization Account to be >0.

d. Major Multi. Contact has a total of 10 or more Sponsorships with Status <> Closed).
Report: "Delinquency: Major Multi Donor Contacts"
Requires the # Active Delinquent Sponsorships value on the Contact to be >0 and requires the # Active Sponsorships on the Contact to be >0

NOTE: E+F are case reports and cannot display Recurring Donation details
e. Open Cases. Contact has any Cases where Status <> Closed
Report: "Delinquency: Donor Contacts w Open Cases"
Requires the # Active Delinquent Sponsorships value on the Contact to be >0 and requires there to be at least one open case linked to that Contact.

f. Participant Incidents. Contact has a Case with Case Record Type = Participant Incident and Case Status <> Closed
Report: "Delinquency: Donor Contacts w Part Incid"
Requires the # Active Delinquent Sponsorships value on the Contact to be >0 and requires there to be at least one open case with a record type of Participant Incident linked to that Contact.

g. Sponsorship is paused (see CFD-2831)
Report: "Delinquency: Donor Contact Paused Giving"
Requires the # Active Delinquent Sponsorships value on the Contact to be >0 and one of the recurring donations linked to that donor contact to have a suspended start date <= Today AND a suspended end date >= today (the giving has been suspended).

h. Do not call. Contact Phone Opt-Out = Yes. 
Report: "Delinquency: Donor Contact Do Not Phone"
Requires the # Active Delinquent Sponsorships value on the Contact to be >0 and the Contact's Phone Opt-Out checkbox to be checked (in the Comm Preferences sub tab).

i. Do not email. Contact 'Email Opt-Out' = Yes 
Report: "Delinquency: Donor Contact Do Not Email"
Requires the # Active Delinquent Sponsorships value on the Contact to be >0 and the Contact's Email Opt-Out checkbox to be checked (in the Comm Preferences sub tab).

j. Payment Method = Benevity 
Report: "Delinquency: Donor Contact Benevity"
Requires the # Active Delinquent Sponsorships value on the Contact to be >0 and one or more active Recurring Donations linked to that contact to have a Payment Method picklist value of Benevity selected.

k. Legacy sponsorships. Sponsorship Legacy Type <> null
Report: "Delinquency: Donor Contact Legacy"
Requires the # Active Delinquent Sponsorships value on the Contact to be >0 and one or more active Recurring Donations linked to that contact to have a Legacy Type value of Term or Perpetual selected.

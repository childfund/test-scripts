https://childfundinternational.atlassian.net/browse/CFD-2831
Donation Update Screen - Suspend Functionality
Sprint28

OVERVIEW
A. Create or select a Donor Contact
B. Create a Sponsorship for the Donor, or open an existing active Sponsorship.
C. Create Designated Funds gift against the Sponsorships
D. Create a Non-Sponsorship recurring gift
E. Suspend each of the gifts

TESTING
A-D. Log in as a Sponsor Care user and locate existing, or create new records.
E. Suspend differnent types of gifts.
- Note: This story applies the suspense to the recurring gift and moves any pending installment opportunities to closed. Currently, NPSP immediately creates the "next" installment. A follow-up story will prevent the "next" installment from being created if it is still within the suspense period.

A. CREATE A DONOR
Log in as a Sponsor Care user
1. Click on the 'Global Actions' menu ("plus-sign in a box" in the upper-right of the site. Or use the [New] button in the 'Contacts' tab).
1.1. Select 'New Contact'.
1.2. OR use the [New] button in the 'Contacts' tab
2. Enter the required fields.
2.2. Click [Next].
2.3. If prompted by the Experian Address Check, select an address option.

B. CREATE A SPONSORSHIP
Continue as a Sponsor Care user
1. Open or create a donor Contact.
2. Click [New Sponsorship/Donation] at the top of the Contact page.
2.1. Select 'Sponsorship (Recurring Donation)' and click [Next].
2.2. Select either 'Automatic Selection (No Preferences)' or 'Find a Child' and click [Next].
2.2.1. If 'Find a Child', select a Participant and click [Next].
2.2. Fill in the required fields and click [Next].
2.3. Click [Finish].

C. CREATE A DESIGNATED FUND
Continue as a Sponsor Care user
1. Open the donor Contact with one or more Sponsorships
2. Click [New Sponsorship/Donation] at the top of the Contact page.
2.1. Select 'Designated Fund (One-Time or Recurring)' and click [Next].
2.2. For 'Designated Fund Type', select a type, and click [Next].
2.3. Fill in the required fields.
2.3.1. For 'Giving Frequency Options', do not select "One-Time". 
2.4. Check one or more Sponsorships.
2.5. Click [Next].

D. CREATE A RECURRING NON-SPONSORSHIP DONATION
Continue as a Sponsor Care user
1. Open the donor Contact
2. Click [New Sponsorship/Donation] at the top of the Contact page.
2.1. Select 'Non-Sponsorship (Recurring Donation)' and click [Next].
2.2. Fill in the required fields.
2.3. Click [Next].

E. SUSPEND RECURRING GIFT
Continue as a Sponsor Care user
1. Open the 'Recurring Donations' tab.
1.2. Select the list view "Open Sponsorships". (note, not using the Sponsorship from Section B, because it has not yet received any payments and does not have an initial installment).
1.3. Click to open one of the recurring donations.
2. Click on the [Manage] button at the top of the page.
2.1. For 'Select an Option', select 'Suspend', and click [Next].
2.2. Confirm date validation
2.2.1. For 'Suspend Start Date' enter a date in the past.
2.2.2. For 'Suspend End Date', enter a date greater than 4 months in the future.
2.2.3. Click [Next].
2.2.4. Confirm the error messages.
2.2.5. Reenter the two date fields within the validation parameters, and click [Next].
3. Confirm the suspense is applied to the gift.
3.1. On the Recurring Donation's 'Details' sub-tab, confirm 'Suspended Start Date' and 'Suspended End Date' are populated correctly. 
4. Confirm the current installment Opportunity is suspended. 
4.1. Select the Recurring Donation's 'Opportunities' sub-tab.
4.2. Confirm the currently due installment is in the 'Closed' list with a 'Stage' = "Suspended".
5. Repeat with the Designated Fund from Section C.
6. Repeat with the Contribution from Section D.

// https://childfundinternational.atlassian.net/browse/CFD-1743
// Security Model: Manager can share created list views
// Sprint9


A. Open Connect Console
1. Log in as a 'Sponsor Care Manager' user.
2. Open the 'Connect Console' app (through the app selector (waffle icon) in the page upper-left).
3. Open the 'Cases' tab.

B. Create and Share a List View with IO users
1. Select 'New' from the 'List View Controls' (gear icon in the upper-right of the list page, next to the list search).
2. Enter a 'List Name'.
3. Select 'Share list view with a group of users' from the 'Who sees this list view?' selections.
3.1. Choose 'Public Groups'.
3.2. Select 'All Internal Users'.
4. Click 'Save'

C. Change List View sharing
1. Select 'Sharing Settings' from the 'List View Controls'
2. Update the 'Who sees this list view?' and/or 'Share list view with groups of users' selections to share with other groups or individuals.


Resources
Salesforce Help "Create or Clone a List View in Lightning Experience": https://help.salesforce.com/articleView?id=sf.customviews_lex.htm&type=5
Trailhead "Create and Customize List Views": https://trailhead.salesforce.com/en/content/learn/modules/lex_customization/lex_customization_list

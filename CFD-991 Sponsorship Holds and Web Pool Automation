// https://childfundinternational.atlassian.net/browse/CFD-991
// Sponsorship Holds and Web Pool Automation
// Sprint10

A. Open an Active Community Account with Available Participants.
1. Log in as a 'Sponsorship' user.
2. View the 'Participants' tab.
3. View the 'Active' list view.
4. Select the Community of one of the Participants.
4.1. Confirm the Community Account has a Status of 'Active'. If not, choose a different Community.
4.2. Confirm 'Used on Web' is checked and 'Web Unavailability Reason' is blank. If not, choose a different Community.
5. Click on the 'Participants' sub-tab.
5.1. Confirm some or all of the Participants are in status 'Active'. If not, choose a different Community.
6. Validate 'Used on Web' is not editable. 

B. Set Web Availability
1. Click the 'Manage Account' button in the upper right.
1.1. Select an Action Type 'Web Availability' and click 'Next'.
1.2. Note the current value: true or false
1.3. Enter a 'Web Unavailability Reason'. 
1.4. Click 'Next'.
2. Validate 'Used on Web' checkbox is unchecked and 'Web Unavailibilty Reason' is populated.
3. Repeat the 'Manage Account' process to set 'Used on Web' back to checked - with a reason.

C. Place Hold on Community and Participants
1. Click the 'Manage Account' button in the upper right.
1.1. Select an Action Type 'Hold Status' and click 'Next'.
1.2. Select 'Hold (All Activity)'.
1.3. Enter a 'Hold Reason'.
1.4. Click 'Next'.
2. Validate 'Used on Web' checkbox is un-checked and 'Web Unavailibilty Reason' is populated with 'Placed on Hold'.
3. Click on the 'Participants' sub-tab.
3.1. Confirm previously 'Available' Participants are now in status 'On Hold' or 'Unavailable' (specific status depends on Participant details).

D. Release Hold on Community and Participants
1. Click the 'Manage Account' button in the upper right.
1.1. Select an Action Type 'Hold Status' and click 'Next'.
1.2. Select 'Yes' for 'Would you like to make this Account available for Web?'.
1.3. Click 'Next'.
2. Validate 'Used on Web' checkbox is checked and 'Web Unavailibilty Reason' is blank.
3. Click on the 'Participants' sub-tab.
3.1. Confirm previously 'On Hold' Participants are now in status 'Available'. If necessary, refresh the list to see new values.

MEC
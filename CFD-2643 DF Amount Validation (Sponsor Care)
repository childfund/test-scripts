https://childfundinternational.atlassian.net/browse/CFD-2643
DF Amount Validation (Sponsor Care)
Sprint28

A. CREATE A DONOR
Log in as a Sponsor Care user
1. Click on the 'Global Actions' menu ("plus-sign in a box" in the upper-right of the site. Or use the [New] button in the 'Contacts' tab).
1.1. Select 'New Contact'.
1.2. OR use the [New] button in the 'Contacts' tab
2. Enter the required fields.
2.2. Click [Next].
2.3. If prompted by the Experian Address Check, select an address option.

B. CREATE A SPONSORSHIP
Continue as a Sponsor Care user
1. Open or create a donor Contact.
2. Click [New Sponsorship/Donation] at the top of the Contact page.
2.1. Select 'Sponsorship (Recurring Donation)' and click [Next].
2.2. Select either 'Automatic Selection (No Preferences)' or 'Find a Child' and click [Next].
2.2.1. If 'Find a Child', select a Participant and click [Next].
2.2. Fill in the required fields and click [Next].
2.3. Click [Finish].

C. CREATE A DESIGNATED FUND
Continue as a Sponsor Care user
1. Open the donor Contact with one or more Sponsorships
2. Click [New Sponsorship/Donation] at the top of the Contact page.
2.1. Select 'Designated Fund (One-Time or Recurring)' and click [Next].
2.2. For 'Designated Fund Type', select a type, and click [Next].
2.3. Fill in the required fields.
2.3.1. For 'Amount', enter $0.
2.3.2. For 'Giving Frequency Options'.
2.4. Check one or more Sponsorships.
2.5. Click [Next].
2.6. Confirm an error message for the $0 is returned.
2.7. For 'Amount', enter an amount > $1,000.
2.8. Click [Next].

D. DF WAIVER
Continue as a Sponsor Care user
1. Open the Designated Fund created in Section C.
2. Confirm there is a DF Waiver message prominently displayed.
3. Confirm 'DF Waiver' is displayed with the option "Required".
3.1. Confirm that 'DF Waiver' is not editable. 

E. UPDATE DF WAIVER
Log in as a user with the Permission Set "Opportunity - DF Wavier Edit" (CFI Admin, Leslie Monk).
1. Open the Designated Fund created in Section C.
2. Confirm that 'DF Waiver' IS editable. 
3. Click the pencil icon next to 'DF Waiver'.
3.1. Change 'DF Waiver' to "None" or "Received".
3.2. Click [Save].
4. Confirm the warning message is no longer on the page.
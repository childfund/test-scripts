// https://childfundinternational.atlassian.net/browse/CFD-406
// Case Automation - "Case Notification - No Activity"
// Sprint7

Create a Country Office type of case (usually a child case under a Master, but can be created on it's own for testing). Assign it to any Country Office Queue and set the status to Pending - Country office.
Go to setup-> Environments -> Monitoring -> Time-Based Workflow and search for Workflow Rule, Flow, or Process Name = Case Notification - No Activity and there should be a new record set to fire 5 days from now.

A. Open Connect Console
1. Log in as a Sponsor Care user.
2. Open the 'Connect Console' app (through the app selector (waffle icon) in the page upper-left).

B. Create a Master Case 
1. Create or Select a Contact.
2. Click 'Create Master Case' at the top of the contact screen.
2.1. Fill in the required fields.
2.2. Set 'Create Routed Cases?' to 'Yes'
2.2. Click 'Next'.

C. Create a Related Country Office Case - Participant
1. Continue with the case creation
1.1. Select '4 - Country Office Request' and click 'Next'.
1.2. For 'Case Related To' select 'Participant'.
1.3. Select a Participant. Example: Raffi Putra Sakhi (That participant has a country office of Indonesia, to which the test 'Country Office' user is a member). 
1.3. Fill in required fields.
1.4. Click 'Next'
2. For '*Would You Like to Create Another Case?', select 'No', and click 'Next'.

D. Set the Case Status
1. Open the Related Country Office case.
2. Open the 'Details' sub-tab.
3. Click the pencil icon next to 'Status'.
3.1. Change 'Status' to 'Pending - Country Office'. 
3.2. Click 'Save'.

E. Check for Scheduled Email
1. Log in as an Admin user.
2. In Setup go to Environments -> Monitoring -> Time-Based Workflow
3. Search for a Workflow Rule, Flow, or Process Name named 'Case Notification - No Activity'
4. Validate the record is scheduled to send in 5 days. 